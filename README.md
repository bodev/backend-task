## Introduction ##

Hello, We are really glad, that you are interested in our job offer.

To test your skills, we found interesting task which we want you to solve.

## Rules ##

* You can use any programming language

## Task ##

Create script that will be able to validate any given string with the following rules:

* all open brackets MUST be closed

    * ✅ ()[]{} [{{}()[]}(){}[()]]
    * ❌ ([{])
    * ❌ ())

* bracket groups MUST NOT cross each other

    * ✅ ([])
    * ❌ ([)]

## Bonus ##

If you think task was too easy you can also implement:

* possibility to escape any type of bracket with backslash, but two backslashes are considered as escaped backslash

    * ✅ ([\\)])
    * ❌ ([\\\\{])

* Unit tests as a proof that your code is working

## Delivery ##

( Create git repository (github, bitbucket...) with your code
`OR`
pack your code to .zip )
`AND`
send link or file to miroslav.kostka@dev.boataround.com and to pavel@boataround.com

## Last words ##

Thank you very much for the time you spend with this test, we really appreciate it! Good luck and in case of any questions/issues, please forward the e-mail to miroslav.kostka@dev.boataround.com